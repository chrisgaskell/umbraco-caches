﻿using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace UmbracoCaches.Website.Controllers
{
    public class OutputCacheController : RenderMvcController
    {
        [OutputCache(Duration = 10)]
        public override ActionResult Index(RenderModel model)
        {
            // This page renders a partial with a thread sleep of 5000ms

            return base.Index(model);
        }
    }
}