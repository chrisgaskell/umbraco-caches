﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace UmbracoCaches.Website.Controllers.Surface
{
    public class DonutSurfaceController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Timestamp()
        {
            return PartialView("_Timestamp"); //located in ~/views/donutsurfacecontroller/_Timestamp.cshtml
        }
    }
}