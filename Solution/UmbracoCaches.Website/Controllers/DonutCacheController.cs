﻿using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace UmbracoCaches.Website.Controllers
{
    public class DonutCacheController : RenderMvcController
    {
        [DonutOutputCache(Duration = 3600)]
        public override ActionResult Index(RenderModel model)
        {
            return base.Index(model);
        }
    }
}