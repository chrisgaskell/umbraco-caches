﻿using System.Web.Mvc;
using Umbraco.Core;

namespace UmbracoCaches.Website
{
    public class UmbracoApplication : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            RegisterGlobalFilters(GlobalFilters.Filters);
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
        }
    }
}