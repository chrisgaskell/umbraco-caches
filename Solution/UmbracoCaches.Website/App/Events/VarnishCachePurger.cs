﻿using System;
using System.Net;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace UmbracoCaches.Website.App.Events
{
    public class VarnishCachePurger : ApplicationEventHandler
    {
        private const string VarnishHost = "varnish.umbracocaches.detangled-digital.com";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            ContentService.Published += ContentServiceOnPublished;
            ContentService.Deleted += ContentServiceOnDeleted;
            ContentService.Moved += ContentServiceOnMoved;
        }

        private static void ContentServiceOnPublished(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            foreach (IContent item in e.PublishedEntities)
            {
                PurgeUrl(library.NiceUrl(item.Id));
            }
        }

        private static void ContentServiceOnDeleted(IContentService sender, DeleteEventArgs<IContent> e)
        {
            foreach (IContent item in e.DeletedEntities)
            {
                PurgeUrl(library.NiceUrl(item.Id));
            }
        }

        private static void ContentServiceOnMoved(IContentService sender, MoveEventArgs<IContent> e)
        {
            foreach (var item in e.MoveInfoCollection)
            {
                PurgeUrl(library.NiceUrl(item.Entity.Id));
            }
        }

        private static void PurgeUrl(string url)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(url)) return;
                string requestUrl = string.Format("http://{0}/{1}", VarnishHost, PrepareUrl(url));
                
                var webRequest = WebRequest.Create(requestUrl) as HttpWebRequest;
                if (webRequest == null)
                    throw new NullReferenceException(string.Format("Cannot create webrequest for url:{0}", url));
                webRequest.Method = "PURGE";
                using (webRequest.GetResponse())
                {
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof (VarnishCachePurger), "Error Purging Url", ex);
            }
        }

        private static string PrepareUrl(string url)
        {
            return url.TrimStart('/');
        }
    }
}