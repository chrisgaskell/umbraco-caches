﻿using System;
using DevTrends.MvcDonutCaching;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;

namespace UmbracoCaches.Website.App.Events
{
    public class OutputCacheClearer : ApplicationEventHandler
    {
        public OutputCacheClearer()
        {
            content.AfterRefreshContent += ContentServiceOnEvent;
            ContentService.Published += ContentServiceOnEvent;
            ContentService.Deleted += ContentServiceOnEvent;
            ContentService.Moved += ContentServiceOnEvent;
        }

        private static void ContentServiceOnEvent(object sender, object e)
        {
            ClearCache();
        }

        private static void ClearCache()
        {
            try
            {
                var cacheManager = new OutputCacheManager();
                cacheManager.RemoveItems(); //Clear all donut output cache
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof (OutputCacheClearer), "Error Clearing Cache", ex);
            }
        }
    }
}