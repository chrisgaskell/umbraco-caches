﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;

namespace UmbracoCaches.Website.App
{
    // @CGaskell requires that a stopwatch has been added to HttpContext.Current.Items

    public class SuperchargerOMeterFilter : Stream
    {
        private readonly Stream _sink;

        public SuperchargerOMeterFilter(Stream sink)
        {
            _sink = sink;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override long Length
        {
            get { return 0; }
        }

        public override long Position { get; set; }

        public override void Flush()
        {
            _sink.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _sink.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _sink.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _sink.SetLength(value);
        }

        public override void Close()
        {
            _sink.Close();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            var stopwatch = HttpContext.Current.Items["Stopwatch"] as Stopwatch;

            if (stopwatch == null) return;

            stopwatch.Stop();

            double percentageOfFive = Math.Ceiling((stopwatch.Elapsed.TotalMilliseconds/8000) * 100);

            string superchargerometer = string.Format(@"
                <div class=""container"">
                    <div class=""well"">
                        <h5 style=""color:#000"">Supercharger-o-meter</h5>
                        <div class=""progress"">
                            <div class=""progress-bar progress-bar-{2}"" role=""progressbar"" aria-valuenow=""{0}"" aria-valuemin=""0"" aria-valuemax=""100"" style=""width: {0}%""></div>
                        </div>
                        <div class=""alert alert-{2}"">
                            <strong>{1}ms load time.</strong> {3}
                        </div>
                    </div>
                </div>",
                percentageOfFive,
                stopwatch.Elapsed.TotalMilliseconds.ToString("N0"),
                stopwatch.Elapsed.TotalMilliseconds < 1000 ? "success" : "warning",
                stopwatch.Elapsed.TotalMilliseconds < 1000
                    ? "Looks like this page has been super charged!"
                    : ":( Supercharging required...");


            var data = new byte[count];
            Buffer.BlockCopy(buffer, offset, data, 0, count);
            string html = Encoding.Default.GetString(buffer);

            html = html.Replace("<!--supercharger-o-meter-->", superchargerometer);

            byte[] outdata = Encoding.Default.GetBytes(html);
            _sink.Write(outdata, 0, outdata.GetLength(0));
        }
    }
}