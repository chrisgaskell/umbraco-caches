﻿using System;
using System.Diagnostics;
using System.Web;

namespace UmbracoCaches.Website.App
{
    public class SuperchargerOMeterModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += OnBeginRequest;
        }

        private static void OnBeginRequest(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            HttpContext.Current.Items["Stopwatch"] = stopwatch;
            stopwatch.Start();

            var app = sender as HttpApplication;
            if (app != null) app.Response.Filter = new SuperchargerOMeterFilter(app.Response.Filter);
        }
    }
}