# Umbraco Caches

Umbraco Caches is a simple website running on an Umbraco V7 CMS.

The goal of the project is to showcase what's possible with Umbraco V7,

If you'd like to take this approach one step further then take a look at the [Gems Cafe project](https://bitbucket.org/chrisgaskell/gemscafe).

Get in touch [@CGaskell](https://twitter.com/CGaskell), [Detangled-Digital.com](http://detangled-digital.com/)

Note: The example uses an embedded SQL Server Compact database and has media items under version control - this is to enable the project to be distributed. You may not want to version control these items in your own solution.


## Site Setup

* Checkout the master branch
* Build the solution inside Visual Studio
* Run PowerShell as an administrator. Run 'Set-ExecutionPolicy RemoteSigned'
* Run the '.\install.ps1' PowerShell script. This will setup IIS, disk permissions and update the hosts file
* Navigate to [http://umbracocaches.local/](http://umbracocaches.local/)

_Note: If you run PowerShell scripts in windows 8 you'll need to set PowerShell to version 2 by opening PowerShell (run as admin) then executing 'powershell -version 2.0'_

## Umbraco 

### URLs

*  [http://umbracocaches.local/](http://umbracocaches.local/) - homepage.
*  [http://umbracocaches.local/umbraco/](http:/umbracocaches.local/umbraco/) - CMS Backoffice.

### CMS User Account
* User: Admin           
* Pass: admin